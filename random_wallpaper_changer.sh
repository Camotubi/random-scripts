#!/bin/sh
PID=$$
ps ax | grep "random_wallpaper_changer.sh" | awk '{print $1}' | grep -v "$PID" | xargs kill
while sleep $1; do \ls ~/Pictures/Wallpapers/ | sort -R | tail -1 | xargs -I {} feh --bg-scale ~/Pictures/Wallpapers/{}; done

