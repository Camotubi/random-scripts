#!/bin/bash
# Rofi firefox bookmarks shell script

URL_REGEX='((http|https):\/\/)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)'

pipe_if_not_empty () {
  head=$(dd bs=1 count=1 2>/dev/null; echo a)
  head=${head%a}
  if [ "x$head" != x"" ]; then
    { printf %s "$head"; cat; } | "$@"
  fi
}

find_default_profile () {
	LAST_PATH=""
	FOUND_DEFAULT=0
	while IFS= read -r line
	do
		FIRST_CHAR=$(echo $line | cut -c 1-1)
		if [ "$FIRST_CHAR" = '[' ];
		then
			LAST_PATH=""
		else
			KEY=$(echo $line | awk -F "=" '{print $1}')
			VAL=$(echo $line | awk -F "=" '{print $2}')
			if [ "$KEY" = "Path" ];
			then
				LAST_PATH=$VAL
			fi
			if [ "$KEY" = "Default" ] && [ "$VAL" -eq 1 ] && [ "$LAST_PATH" != "" ];
			then
				break
			fi
		fi
	done < "$1"
	echo $LAST_PATH
}

DEFAULT_PROFILE_DIRECTORY=$(find_default_profile ~/.mozilla/firefox/profiles.ini)

cp -f "$HOME/.mozilla/firefox/$DEFAULT_PROFILE_DIRECTORY/places.sqlite" /tmp/firefox_bookmarks.sqlite

sqlite3 /tmp/firefox_bookmarks.sqlite  '
select distinct moz_bookmarks.title, url 
    from moz_bookmarks left join moz_places 
    on moz_places.id = moz_bookmarks.fk 
    order by visit_count desc;'\
| grep -E $URL_REGEX \
| awk -F "|" '{printf "%d:\t%s\t%s\n", NR, substr($1, 1,20), $2}' \
| rofi -dmenu -location 6 -width 100 \
		 -line-margin 0 -line-padding 1 \
		 -separator-style none -font "mono 10" -columns 9 -bw 0 \
		 -disable-history \
		 -hide-scrollbar \
		 -color-window "#222222, #222222, #b1b4b3" \
		 -color-normal "#222222, #b1b4b3, #222222, #005577, #b1b4b3" \
		 -color-active "#222222, #b1b4b3, #222222, #007763, #b1b4b3" \
		 -color-urgent "#222222, #b1b4b3, #222222, #77003d, #b1b4b3" \
		 -kb-row-select "Tab" -kb-row-tab "" \
| awk -F '\t' '{printf "%s", $3}'\
| pipe_if_not_empty xargs firefox --new-tab

